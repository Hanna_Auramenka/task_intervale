package com.example.test_task.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "magazine")
public class Magazine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private String name;

    @Column
    private LocalDate date;

    @Column
    private String author;

    @Column
    private String publicingOffice;

    public Magazine() {
    }

    public Magazine(String name, LocalDate date, String author, String publicingOffice) {
        this.name = name;
        this.date = date;
        this.author = author;
        this.publicingOffice = publicingOffice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublicingOffice() {
        return publicingOffice;
    }

    public void setPublicingOffice(String publicingOffice) {
        this.publicingOffice = publicingOffice;
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", date=" + date +
                ", author='" + author + '\'' +
                ", publicingOffice='" + publicingOffice + '\'' +
                '}';
    }
}
