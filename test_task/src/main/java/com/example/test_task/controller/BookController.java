package com.example.test_task.controller;

import com.example.test_task.model.Book;
import com.example.test_task.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    @PostMapping("/books")
    public ResponseEntity<Book> createBook(@RequestBody Book book){
        return new ResponseEntity<Book>(bookService.saveBook(book), HttpStatus.CREATED);
    }

    @DeleteMapping("/books/{id}")
    public ResponseEntity<String> deleteBook(@PathVariable("id") Long id){
        bookService.deleteBook(id);
        return new ResponseEntity<String>("Deleted", HttpStatus.OK);
    }

    @GetMapping("/books")
    public List<Book> getAllBooks(){
        return bookService.getAllBooks();
    }

    @GetMapping("/books/{author}")
    public ResponseEntity<List<Book>> getBookByAuthor(@PathVariable("author") String author){
        List<Book> books = bookService.getBookByAuthor(author);
        return new ResponseEntity<List<Book>>(books, HttpStatus.OK);
    }

    @PutMapping("/books/{id}")
    public ResponseEntity<Book> updateBook(@RequestBody Book book, @PathVariable("id") Long id){
        book.setId(id);
        return new ResponseEntity<Book>(bookService.updateBook(book), HttpStatus.CREATED);
    }
}
