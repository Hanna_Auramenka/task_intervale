package com.example.test_task.controller;

import com.example.test_task.model.Magazine;
import com.example.test_task.service.MagazineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MagazineController {

    @Autowired
    private MagazineService magazineService;

    @PostMapping("/magazines")
    public ResponseEntity<Magazine> createMagazine(@RequestBody Magazine magazine){
        return new ResponseEntity<Magazine>(magazineService.saveMagazine(magazine), HttpStatus.CREATED);
    }

    @DeleteMapping("/magazines/{id}")
    public ResponseEntity<String> deleteMagazine(@PathVariable("id") Long id){
        magazineService.deleteMagazine(id);
        return new ResponseEntity<String>("Deleted", HttpStatus.OK);
    }

    @GetMapping("/magazines")
    public List<Magazine> getAllMagazines(){
        return magazineService.getAllMagazines();
    }

    @GetMapping("/magazines/{author}")
    public ResponseEntity<List<Magazine>> getMagazineByAuthor(@PathVariable("author") String author){
        List<Magazine> magazines = magazineService.getMagazineByAuthor(author);
        return new ResponseEntity<List<Magazine>>(magazines, HttpStatus.OK);
    }

    @PutMapping("/magazines/{id}")
    public ResponseEntity<Magazine> updateMagazine(@RequestBody Magazine magazine, @PathVariable("id") Long id){
        magazine.setId(id);
        return new ResponseEntity<Magazine>(magazineService.updateMagazine(magazine), HttpStatus.CREATED);
    }
}
