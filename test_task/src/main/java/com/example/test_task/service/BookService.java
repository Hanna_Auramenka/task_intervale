package com.example.test_task.service;

import com.example.test_task.exception.BookNotExistsException;
import com.example.test_task.model.Book;
import com.example.test_task.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    @Autowired
    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Book saveBook(Book book){
        return bookRepository.save(book);
    }

    public Book updateBook(Book book){
        Optional<Book> bookOptional = bookRepository.findById(book.getId());

        if (!bookOptional.isPresent()){
            throw new BookNotExistsException("Book doesn't exist");
        }

        Book book1 = bookOptional.get();
        book1.setId(book.getId());
        book1.setName(book.getName());
        book1.setDate(book.getDate());
        book1.setAuthor(book.getAuthor());
        book1.setPublicingOffice(book.getPublicingOffice());

        return bookRepository.save(book1);
    }

    public List<Book> getAllBooks(){
        List<Book> books = bookRepository.findAll();

        if (books.isEmpty()){
            throw new BookNotExistsException("Books no found");
        }

        return books;
    }

    public List<Book> getBookByAuthor(String author){
        List<Book> bookList = bookRepository.findBookByAuthor(author);
        if (bookList.isEmpty()){
            throw new BookNotExistsException("Books don't found with this author");
        }
        return bookList;
    }

    public void deleteBook(Long id){
        Optional<Book> optional = bookRepository.findById(id);
        if (!optional.isPresent()){
            throw new BookNotExistsException("Book doesn't found with this id");
        }else {
            bookRepository.delete(optional.get());
        }
    }
}
