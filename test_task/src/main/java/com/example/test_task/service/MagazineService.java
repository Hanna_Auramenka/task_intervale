package com.example.test_task.service;

import com.example.test_task.exception.MagazineNotExistsException;
import com.example.test_task.model.Magazine;
import com.example.test_task.repository.MagazineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MagazineService {

    @Autowired
    private final MagazineRepository magazineRepository;

    public MagazineService(MagazineRepository magazineRepository) {
        this.magazineRepository = magazineRepository;
    }

    public Magazine saveMagazine(Magazine magazine){
        return magazineRepository.save(magazine);
    }

    public Magazine updateMagazine(Magazine magazine){
        Optional<Magazine> magazineOptional = magazineRepository.findById(magazine.getId());

        if (!magazineOptional.isPresent()){
            throw new MagazineNotExistsException("Magazine doesn't exist");
        }

        Magazine magazine1 = magazineOptional.get();
        magazine1.setId(magazine.getId());
        magazine1.setName(magazine.getName());
        magazine1.setDate(magazine.getDate());
        magazine1.setAuthor(magazine.getAuthor());
        magazine1.setPublicingOffice(magazine.getPublicingOffice());

        return magazineRepository.save(magazine1);
    }

    public List<Magazine> getAllMagazines(){
        List<Magazine> magazines = magazineRepository.findAll();

        if (magazines.isEmpty()){
            throw new MagazineNotExistsException("Magazines no found");
        }

        return magazines;
    }

    public List<Magazine> getMagazineByAuthor(String author){
        List<Magazine> magazineList = magazineRepository.findMagazineByAuthor(author);
        if (magazineList.isEmpty()){
            throw new MagazineNotExistsException("Magazines don't found with this author");
        }
        return magazineList;
    }

    public void deleteMagazine(Long id){
        Optional<Magazine> optional = magazineRepository.findById(id);
        if (!optional.isPresent()){
            throw new MagazineNotExistsException("Magazine doesn't found with this id");
        }else {
            magazineRepository.delete(optional.get());
        }
    }
}
