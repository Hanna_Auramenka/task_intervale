package com.example.test_task.exception;

public class MagazineNotExistsException extends RuntimeException{
    public MagazineNotExistsException(String message) {
        super(message);
    }
}
