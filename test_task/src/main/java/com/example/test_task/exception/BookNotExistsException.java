package com.example.test_task.exception;

public class BookNotExistsException extends RuntimeException{
    public BookNotExistsException(String message) {
        super(message);
    }
}
